import Page from './Page.js';
import Component from '../components/Component';

export default class AddPizzaPage extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
		const form = document.querySelector('form');
		form.addEventListener('submit', event => {
			event.preventDefault();
			const input = document.querySelector('input[name=name]');
			if (input.value == '') {
				alert('Pas de valeur saisie');
			} else alert('La pizza ' + input.value + ' a été ajoutée ! ');
		});
	}

	submit(event) {}
}
