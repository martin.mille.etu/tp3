import Page from './Page.js';
import Component from '../components/Component.js';
import PizzaThumbnail from '../components/PizzaThumbnail.js';

export default class PizzaList extends Page {
	#pizzas;

	constructor(pizzas) {
		super('pizzaList');
		this.pizzas = pizzas;
	}

	set pizzas(value) {
		this.#pizzas = value;
		this.children = this.#pizzas.map(pizza => new PizzaThumbnail(pizza));
	}
}
