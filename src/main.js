import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component';
import PizzaForm from './pages/PizzaForm';
import Page from './pages/Page';

//Router.menuElement = document.querySelector('.mainMenu');

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList([]),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();

Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];


window.onpopstate = function(event) {
	event.preventDefault();
	let path = document.location.pathname
	Router.navigate(path);
};





Router.menuElement = document.querySelector('.mainMenu');
Router.navigate(document.location.pathname); // affiche une page vide
pizzaList.pizzas = data;

// affiche la liste des pizzas
//const elementsArray = document.querySelectorAll('.pizzaThumbnail h4');
//console.log(elementsArray[1]);
const logo = document.querySelector('.logo');
logo.innerHTML += `
<small> les pizzas c'est la vie </small> `;

const link = document.querySelectorAll('footer div a');
console.log(link[1].getAttribute('href'));

const elem = document.querySelector('.newsContainer');
elem.style = 'display:';

const button = document.querySelector('.closeButton');
button.addEventListener('click', event => {
	event.preventDefault();
	const elem = document.querySelector('.newsContainer');
	elem.style = 'display:none';
});
