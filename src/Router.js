export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static currentLink;
	static navigate(path) {
		//window.history.go
		if (this.currentLink != null) {
			this.currentLink.classList.remove('class', 'active');
		}
		const donnee = document.querySelectorAll('.mainMenu a');
		donnee.forEach(element => {
			const lien = element.getAttribute('href');
			if (lien == path) {
				this.currentLink = element;
			}
		});

		this.currentLink.classList.add('class', 'active');

		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			if (route.page.mount) route.page.mount(this.contentElement);
		}
	}

	static #menuElement;
	static set menuElement(element) {
		this.#menuElement = element;
		const elem = element.querySelectorAll(`a`);
		console.log(elem);
		elem.forEach(element => {
			element.addEventListener('click', event => {
				event.preventDefault();
				window.history.pushState(null,null,element.getAttribute('href')) ;
				Router.navigate(element.getAttribute('href'));
				//console.log(element.getAttribute('href'));
			});
		});
	}
}
